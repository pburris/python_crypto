import string
import numpy

from matplotlib import pyplot
from collections import Counter

def plot_graph(subplot, title, text, color):
    letters, frequency = calculate_frequency(text)
    index = numpy.arange(len(letters))
    bar_width = 0.5

    subplot.bar(index, frequency, bar_width, color=color)
    subplot.set_xticks(index)
    subplot.set_xticklabels(letters)
    subplot.set_ylabel('Frequency')
    subplot.set_title(title)

def calculate_frequency(text):
    text = [c for c in text if c in string.ascii_uppercase]
    frequency = Counter(text)
    listed_items = frequency.items()
    listed_items = sorted(listed_items, key=lambda x: x[1], reverse=True)
    return list(zip(*listed_items))


class TextAnalyzer(object):

    def __init__(self, texts):
        self.texts = texts
        self.fig = None

    def plot(self):
        self.fig = pyplot.figure()

        for i in range(len(self.texts)):
            subplot = self.fig.add_subplot(2, 1, i + 1)
            plot_graph(subplot, "Text Frequency #{}".format(i+1), self.texts[i], ["blue", "green", "red"][i%3])

        pyplot.show()
