'''
Caeser Cypher
'''
from app.tools import ALPHABET

def encrypt(key, plaintext):
    key %= len(ALPHABET)  # Clamp the value of the key between 0 and 25
    ciphertext = ""

    for letter in plaintext:
        shifted_letter = letter
        if letter in ALPHABET:
            index = ALPHABET.index(letter.upper())
            shifted_letter = ALPHABET[(index + key) % 26]
        ciphertext += shifted_letter

    return ciphertext


def decrypt(key, ciphertext):
    key %= len(ALPHABET)  # Clamp the value of the key between 0 and 25
    plaintext = ""

    for letter in ciphertext:
        shifted_letter = letter
        if letter in ALPHABET:
            index = ALPHABET.index(letter.upper())
            shifted_letter = ALPHABET[(index - key) % 26]
        plaintext += shifted_letter

    return plaintext
