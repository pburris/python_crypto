'''
Vigenere Cypher
'''
from app.tools import ALPHABET

def key_maker(key):
    d = { 'pos': len(key) }
    def maker():
        ret = key[d['pos'] % len(key)]
        d['pos'] = d['pos'] + 1
        return ret
    return maker

def padded_key(key, text):
    ret = ''
    maker = key_maker(key)
    for l in text:
        if l not in ALPHABET:
            ret += l
        else:
            ret += maker()
    return ret[0:len(text)]

def encrypt(key, plaintext):
    key = "".join(key.split())
    plaintext = "$".join(plaintext.split())
    ciphertext = ''
    k = padded_key(key, plaintext)

    for i in range(0, len(k)):
        current_key = k[i]
        current_text = plaintext[i]
        if current_key in ALPHABET and current_text in ALPHABET:
            key_index = ALPHABET.index(current_key)
            text_index = ALPHABET.index(current_text)
            shift = (text_index + key_index) % 26
            ciphertext += ALPHABET[shift]
        else:
            ciphertext += current_text

    return " ".join(ciphertext.split('$'))


def decrypt(key, ciphertext):
    key = "".join(key.split())
    ciphertext = "$".join(ciphertext.split())
    plaintext = ''
    k = padded_key(key, ciphertext)

    for i in range(0, len(k)):
        current_key = k[i].upper()
        current_text = ciphertext[i].upper()
        if current_key in ALPHABET and current_text in ALPHABET:
            key_index = ALPHABET.index(current_key)
            text_index = ALPHABET.index(current_text)
            shift = (text_index - key_index) % 26
            plaintext += ALPHABET[shift]
        else:
            plaintext += current_text

    return " ".join(plaintext.split('$'))
