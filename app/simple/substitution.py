'''
Substitution Cypher
'''
from app.tools import ALPHABET

def map_letter(letter, base, to):
    if letter in base:
        return to[base.index(letter)]
    return letter

def encrypt(key, plaintext):
    ciphertext = ''

    for letter in plaintext:
        substituted_letter = map_letter(letter, ALPHABET, key)
        ciphertext += substituted_letter

    return ciphertext


def decrypt(key, ciphertext):
    plaintext = ''

    for letter in ciphertext:
        substituted_letter = map_letter(letter, key, ALPHABET)
        plaintext += substituted_letter

    return plaintext
