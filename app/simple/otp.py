'''
One Time Pad Cypher
'''
import os
import bitarray
from collections import Counter
from binascii import hexlify

def xor(a, b):
    return bytes([c ^ d for c, d in zip(a, b)]).hex()

def encrypt(key, plaintext):
    # Because input is a hex string, we convert to raw bytes to do XOR
    return xor(bytearray.fromhex(key), bytearray.fromhex(plaintext))


def decrypt(a, b):
    # Because input is a hex string, we convert to raw bytes to do XOR
    return bytes([c ^ d for c, d in zip(bytearray.fromhex(a), bytearray.fromhex(b))])


def hex_encode(string):
    """Helper function to encode strings to hexstrings"""
    # Python has flaws, unless i'm doing something wrong..
    return hexlify(string.encode('utf-8')).decode('utf-8')

def hex_decode(hstring):
    return bytes.fromhex(hstring).decode('utf-8')


def print_distribution(text):
    """Print the distribution of bits"""
    if isinstance(text, str):
        text = bytes(text.encode('utf-8'))

    text_bits = bitarray.bitarray()
    text_bits.frombytes(text)
    summary = Counter(text_bits)
    ones = summary[True]
    zeros = summary[False]
    total = ones + zeros
    print("0: {0} ({1}%)".format(zeros, 100 * (float(zeros) / total)))
    print("1: {0} ({1}%)".format(ones, 100 * (float(ones) / total)))


def keys_from_messages(ciphertexts):
    key = {}

    for (c1, c2) in zip(ciphertexts[0::2], ciphertexts[1::2]):
        space_positions = {}
        if c1 != c2:
            result = hex_decode(encrypt(c1, c2))
            for i in range(len(result)):
                if result[i].isspace():
                    if i in space_positions:
                        space_positions[i] += 1
                    else:
                        space_positions[i] = 1

        for spaces in space_positions:
            if space_positions[spaces] == len(ciphertexts) - 1:
                key[i] = c1[i]


    return key


def is_space(c):
    return c == " "


if __name__ == '__main__':
    messages = [
        "396684330550383d3519d2c120a042e01a188136be80f58834436b12ce1a6e887f77249ef4301b03c0371183cba73db2f76aad272da49e5837bba2b8f35f46f9fcfaf58a16a4829ca8d6d6bbf8ea7ef4cfccd4c3265c8904b7f3ca6a88100a7f39d834c3e71766",
        "2c6288220550383d3519decb33a145e014569579ea99b09d28173901c60965c36a7c3a9eb0791a45942c198384ee3dbaa528b06e21e1ad142db1b4ece2450ff8f9fae89653f78fddb0d888e2b9e96fb7c88a9bde330a8f0cf0a7cc60dc0f02687e8c2f8caf1668f4015be0a6b05c835344eb7839c89622e86c6e1cbf18a00b9a66",
        "3164cd310e1f3b34234b90c928a449e007568279bd81f59834593f44ea047ecb6e3e379de4750645893054d7d1ab25b0a528bb6926a4cc1b2bbca2a5f24814fffcbda18a1cf3c094a89d8ef3fca46dbbd2c6908b3442854aa7b2d725881743763b8c60c3b20729e7464ee1adbe",
        "3d63993805026f282e5c90d322a540ae04179536bc8aa79671532e01db4437c7793e2593f53012008c285881dabc2af5a464bb7029b8c05822bda3ece54503b6fabbe5c203e88593b2c4daf4ffa46ebdcdcfd4ca340a9302b5f3d360920c4375318f2e8cb31c29ea4e40e3e3f16c8d4452a2647cc9d73aa67c2148b94ca4019864a540d4430db7cc",
        "3c659a3e4c502b3331579c8423a65be05d56b27eaf9db0cf26563844c50763c0627031dbf57c0700c03017d7dba17ff5a467f44629a88f1d64a1bea3f80d04f3f5bbefc207e58c96afd39dbbf8e37bbdce84d4ef2e448102bcbf8468950b1031339d60daa20170a64c5aebabb07a8d1c48eb6b71cfdb7b81387254b919bf0ad674a85b9a5f44",
        "396684330550202c2357d5c067bd44eb53128979b8cfb48135172d0bde0673887f76378fb07900458c211cd7d6a027baf769f47428a0801464a2b0bfe54c01f3befaef8d07a48d88a5d5daf7f8f67db1d28a80c32644c00bf0a1c571d1100c7d3b",
        "317ecd2701036f3d2a5590d222bb55ae04138a7aea9bbacf22563244ef1a7ec6603e3b9ebc30161094640c9fdaee24bca46df46b2cb5981421f290a0ff4e03b6e5bbf2c21deb94dda1d293f5fea46ebb80ce9b8b3342811ef0baca259d580b642c8a3982",
        "396c993512502e7c3151d9c822e50ce81a18827fa488f59b39563f44c50763c0627031dbfd7f0600c02c1987cfab3db0b324f4742da4cc1c21b1b8a8f34946f9fcfae68d1aea87ddafd38ef4b9f072b180cd95d9234f8e4ab1a7846a921b062a7e9a35d8eb5368ea405ca8a5ff7cc24149ed7e39fa9b32ab7d20",
        "2b62887001042a7c2719dccd33bd40eb53148f62e6cfb48135173805c20c37c965663f94e563181cc03017d7d7ab21a6b264b22b6596841127baf1bbf75459b6c5b2e8811ba4979cbf82d6bbf1eb76b0c9c4938b2f4f924ab8b2ca61dc170d312a90258cb31c79a64e49a8abf57cc25943e368",
        "356b8329400426312319c0c523e94dfa0717857db9cfb49d34173b16ce1c63d12b7d3994fc3e543188215884daad21b0a328b96236b28d1f21f2b8bfb67e0defe0b3ecc231e18c92a8da89bbedeb3aa0c8cfd4e528588419",
    ]

    print(keys_from_messages(messages))
